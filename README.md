## Rakenduse terminalis käivitamine

1. Klooni repo arvutisse "git clone https://jaelle20@bitbucket.org/jaelle20/cosmos-odyssey.git"
2. Navigeeri kausta "Cosmos-Odyssey\WebApp"
3. Käivita käsklusega "dotnet run"
4. Ava brauseris "https://localhost:7211"

---

## Rakenduse kasutamine

1. Avalehelt saab otsida lende lähte- ja sihtkoha järgi või otsida reserveeringuid ees- ja perenime järgi.
2. Lendude tabelis paksus kirjas on lennukombinatsiooni kokkuvõte, selle all lennust see, millest koosneb.
3. Kui sihtkohta saab ümberistumisi tegemata, on lend paksus kirjas.
4. Lende saab filtreerida lennufirmade järgi ja järjestada hinna, lennudistantsi või saabumisaja alusel.
5. Broneeringu tegemisel tuleb sisestada ees- ja perenimi.
6. Broneeringu õnnestumisel kuvatakse kõik sellele nimele tehtud broneeringud.