﻿using System.Text.Json.Serialization;

namespace Domain;

public class Planet
{
    [JsonPropertyName("Id")]
    public string PlanetId { get; set; }
    [JsonPropertyName("Name")]
    public string PlanetName { get; set; }
    
    // [JsonIgnore]
    // public ICollection<RouteInfo> RouteInfos { get; set; }
}