﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain;

public class Reservation
{
    public int ReservationId { get; set; }
    
    [DisplayName("First name")]
    [MaxLength(30)]
    [MinLength(2)]
    public string FirstName { get; set; }
    
    
    [DisplayName("Last name")]
    [MaxLength(30)]
    [MinLength(3)]
    public string LastName { get; set; }
    
    public ICollection<ReservationProvider>? ReservationProviders { get; set; }

    public string PriceListId { get; set; }
    public PriceList? PriceList { get; set; }
}