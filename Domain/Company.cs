﻿using System.Text.Json.Serialization;

namespace Domain;

public class Company
{
    [JsonPropertyName("Id")]
    public string CompanyId { get; set; }
    [JsonPropertyName("Name")]
    public string CompanyName { get; set; }
    
    [JsonIgnore]
    public ICollection<Provider> Providers { get; set; }

}