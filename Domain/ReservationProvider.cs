﻿namespace Domain;

public class ReservationProvider
{
    public int ReservationProviderId { get; set; }
    
    public int ReservationId { get; set; }
    public Reservation Reservation { get; set; }
    public string ProviderId { get; set; }
    public Provider Provider { get; set; }
}