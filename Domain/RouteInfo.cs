﻿using System.Text.Json.Serialization;

namespace Domain;

public class RouteInfo
{
    [JsonPropertyName("Id")]
    public string RouteInfoId { get; set; }
    
    [JsonIgnore]
    public string PlanetFromId { get; set; }
    [JsonPropertyName("From")]
    public Planet PlanetFrom { get; set; }
    
    [JsonIgnore]
    public string PlanetToId { get; set; }
    [JsonPropertyName("To")]
    public Planet PlanetTo { get; set; }
    
    public long Distance { get; set; }
    
    [JsonIgnore]
    public ICollection<Leg> Legs { get; set; }
}