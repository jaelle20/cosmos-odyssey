﻿using System.Text.Json.Serialization;

namespace Domain;

public class Provider
{
    [JsonPropertyName("Id")]
    public string ProviderId { get; set; }
    
    [JsonIgnore]
    public string CompanyId { get; set; }
    public Company Company { get; set; }

    public decimal Price { get; set; }
    public DateTime FlightStart { get; set; }
    public DateTime FlightEnd { get; set; }
    
    [JsonIgnore]
    public string LegId { get; set; }
    [JsonIgnore]
    public Leg Leg { get; set; }
    
    [JsonIgnore]
    public ICollection<ReservationProvider> ReservationProviders { get; set; }
}