﻿using System.Text.Json.Serialization;

namespace Domain;

public class PriceList
{
    [JsonPropertyName("Id")]
    public string PriceListId { get; set; } 
    public DateTime ValidUntil { get; set; }
    public ICollection<Leg> Legs { get; set; }
    
    [JsonIgnore]
    public ICollection<Reservation> Reservations { get; set; }
}