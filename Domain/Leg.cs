﻿using System.Text.Json.Serialization;

namespace Domain;

public class Leg
{
    [JsonPropertyName("Id")]
    public string LegId { get; set; }
    public ICollection<Provider> Providers { get; set; }
    
    [JsonIgnore]
    public string RouteInfoId { get; set; }
    public RouteInfo RouteInfo { get; set; }
    
    [JsonIgnore]
    public string PriceListId { get; set; }
    [JsonIgnore]
    public PriceList PriceList { get; set; }
}