﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages;

public class ReservationForm : PageModel
{
    private readonly DAL.AppDbContext _context;

    public ReservationForm(DAL.AppDbContext context)
    {
        _context = context;
    }

    [BindProperty]
    public Reservation Reservation { get; set; }

    [BindProperty]
    public List<string> FlightIds { get; set; }

    public List<Provider> Flights;

    public async Task<IActionResult> OnGetAsync()
    {
        FlightIds = Request.Query["flightId"].ToList();

        if (FlightIds.Count == 0)
        {
            return NotFound();
        }
        
        InitializeFlights();

        Reservation = new Reservation
        {
            PriceListId = Flights[0].Leg.PriceListId
        };

        return Page();
    }
    
    public async Task<IActionResult> OnPostAsync()
    {
        if (!ModelState.IsValid)
        {
            InitializeFlights();
            return Page();
        }

        Reservation.ReservationProviders = new List<ReservationProvider>();
        foreach (var providerId in FlightIds)
        {
            var reservationProvider = new ReservationProvider()
            {
                ProviderId = providerId
            };
            Reservation.ReservationProviders.Add(reservationProvider);

        }
        _context.Reservations.Add(Reservation);

        await _context.SaveChangesAsync();
        
        var parameters = new RouteValueDictionary
        {
            ["firstName"] = Reservation.FirstName,
            ["lastName"] = Reservation.LastName
        };

        return RedirectToPage("./Reservations", parameters);
    }

    private void InitializeFlights()
    {
        Flights = _context.Providers
            .Include(p => p.Company)
            .Include(p => p.Leg)
                .ThenInclude(l => l.RouteInfo)
                    .ThenInclude(r => r.PlanetFrom)
            .Include(p => p.Leg)
                .ThenInclude(l => l.RouteInfo)
                    .ThenInclude(r => r.PlanetTo)
            .Include(p => p.Leg)
            .Where(p => FlightIds.Contains(p.ProviderId))
            .OrderBy(p => p.FlightStart)
            .ToList();
    }
}