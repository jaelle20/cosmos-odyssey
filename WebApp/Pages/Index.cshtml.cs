﻿using Domain;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages;

public class IndexModel : PageModel
{
    private readonly DAL.AppDbContext _context;

    public IndexModel(DAL.AppDbContext context)
    {
        _context = context;
        Planets = _context.Planets.ToList();
    }

    public string? FirstName;
    public string? LastName;
    public string? Origin;
    public string? Destination;
    
    public ICollection<Planet> Planets;

    public void OnGet(string? origin, string? destination, string? firstName, string? lastName)
    {
        if (origin != null)
        {
            Origin = origin;
        }
        if (destination != null)
        {
            Destination = destination;
        }
        if (firstName != null)
        {
            FirstName = firstName;
        }
        if (lastName != null)
        {
            LastName = lastName;
        }
    }
}