﻿using Domain;
using Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Pages;

public class Flights : PageModel
{
    private readonly DAL.AppDbContext _context;

    public Flights(DAL.AppDbContext context)
    {
        _context = context;
        DataController = new DataController(_context);
        PriceList = DataController.GetActivePriceList();
        FlightFinder = new FlightFinder(PriceList);
        Companies = _context.Companies.ToList();
    }

    public string? Sort;
    public List<string> CheckedCompanies;
    public Planet OriginPlanet;
    public Planet DestinationPlanet;
    public string OriginId;
    public string DestinationId;
    public List<Company> Companies;
    public List<List<Provider>> FlightCombos;
    private DataController DataController;
    public PriceList PriceList;
    private FlightFinder FlightFinder;
    
    public async Task<IActionResult> OnGet(string? origin, string? destination, string? sort)
    {
        if (origin == null || destination == null)
        {
            return RedirectToPage("./Index");
        }

        if (origin == destination)
        {
            var parameters = new RouteValueDictionary
            {
                ["origin"] = origin,
                ["destination"] = destination
            };

            return RedirectToPage("./Index", parameters);
        }

        OriginId = origin;
        DestinationId = destination;
        
        CheckedCompanies = Request.Query["company"].ToList();
        
        OriginPlanet = _context.Planets.FirstOrDefault(p => p.PlanetId == origin);
        DestinationPlanet = _context.Planets.FirstOrDefault(p => p.PlanetId == destination);

        if (OriginPlanet == null || DestinationPlanet == null)
        {
            return RedirectToPage("./Index");
        }

        FlightCombos = FlightFinder.GetAllFlightCombinationsFromTo(OriginPlanet, DestinationPlanet);

        if (CheckedCompanies.Any())
        {
            FlightCombos = FlightCombos.Where(l => l.All(p => CheckedCompanies.Contains(p.Company.CompanyId))).ToList();
        }

        Sort = sort;
        switch (Sort)
        {
            case "shortest":
                FlightCombos = FlightCombos.OrderBy(l => l.Select(p => p.Leg.RouteInfo.Distance).Sum()).ToList();
                break;
            case "soonest":
                FlightCombos = FlightCombos.OrderBy(l => l.Select(p => p.FlightEnd).Max()).ToList();
                break;
            // case "quickest":
            //     FlightCombos = FlightCombos.OrderBy(l => l.Select(p => p.FlightEnd).D l.Select(p => p.FlightStart)).ToList();
            //     break;
            default:
                FlightCombos = FlightCombos.OrderBy(l => l.Select(p => p.Price).Sum()).ToList();
                break;
        }

        return Page();
    }
}