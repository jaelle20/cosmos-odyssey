﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace WebApp.Pages;

public class Reservations : PageModel
{
    private readonly DAL.AppDbContext _context;
    
    public Reservations(DAL.AppDbContext context)
    {
        _context = context;
    }

    public List<Reservation> ReservationList;
    public List<List<Provider>> FlightCombos;

    public async Task<IActionResult> OnGetAsync(string? firstName, string? lastName)
    {
        if (firstName == null || lastName == null)
        {
            return RedirectToPage("./Index");
        }
        
        ReservationList = _context.Reservations
            .Include(r => r.ReservationProviders)!
                .ThenInclude(rp => rp.Provider)
                    .ThenInclude(p => p.Company)
            .Include(r => r.ReservationProviders)!
                .ThenInclude(rp => rp.Provider)
                    .ThenInclude(p => p.Leg)
                        .ThenInclude(l => l.RouteInfo)
                            .ThenInclude(r => r.PlanetFrom)
            .Include(r => r.ReservationProviders)!
                .ThenInclude(rp => rp.Provider)
                    .ThenInclude(p => p.Leg)
                        .ThenInclude(l => l.RouteInfo)
                            .ThenInclude(r => r.PlanetTo)
            .Where(r => r.LastName.ToLower().Equals(lastName.ToLower()) & r.FirstName.ToLower().Equals(firstName.ToLower()))
            .ToList();

        if (!ReservationList.Any())
        {
            var parameters = new RouteValueDictionary
            {
                ["firstName"] = firstName,
                ["lastName"] = lastName
            };
            return RedirectToPage("./Index", parameters);
        }

        FlightCombos = new List<List<Provider>>();
        foreach (var flightCombo in ReservationList
                     .Select(reservation => reservation.ReservationProviders
                         .Select(reservationProvider => reservationProvider.Provider)
                         .OrderBy(p => p.FlightStart)
                         .ToList()))
        {
            FlightCombos.Add(flightCombo);
        }
        
        return Page();
    }
}