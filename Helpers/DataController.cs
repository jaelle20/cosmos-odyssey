﻿using System.Net;
using System.Text.Json;
using System.Text.Json.Serialization;
using DAL;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace Helpers;

public class DataController
{
    private const string TravelPricesApiUrl = "https://cosmos-odyssey.azurewebsites.net/api/v1.0/TravelPrices";
    private readonly AppDbContext _dbContext;

    public DataController(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    private string GetNewPricelistJson()
    {
        string html;
        var request = (HttpWebRequest)WebRequest.Create(TravelPricesApiUrl);
        request.AutomaticDecompression = DecompressionMethods.GZip;

        using HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        using Stream stream = response.GetResponseStream();
        using StreamReader reader = new StreamReader(stream);
        html = reader.ReadToEnd();

        return html;
    }

    private void GetNewPriceList()
    {
        var json = GetNewPricelistJson();
        
        var options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            WriteIndented = true,
            ReferenceHandler = ReferenceHandler.Preserve,
        };
        
        var priceList = JsonSerializer.Deserialize<PriceList>(json, options);
        
        if (priceList == null) return;
        
        var priceListRem = RemoveDuplicateInstances(priceList);

        _dbContext.PriceLists.Add(priceListRem);
        _dbContext.SaveChanges();

        if (_dbContext.PriceLists.Count() > 15)
        {
            DeleteOldestPriceList();
        }
    }

    public PriceList GetActivePriceList()
    {
        if (!_dbContext.PriceLists.Any(p => p.ValidUntil > DateTime.UtcNow))
        {
            GetNewPriceList();
        }
        
        var priceList = _dbContext.PriceLists
            .Include(p => p.Legs)
                .ThenInclude(l => l.Providers)
                    .ThenInclude(p => p.Company)
            .Include(p => p.Legs)
                .ThenInclude(l => l.RouteInfo)
                    .ThenInclude(p => p.PlanetFrom)
            .Include(p => p.Legs)
                .ThenInclude(l => l.RouteInfo)
                    .ThenInclude(p => p.PlanetTo)
            .OrderByDescending(p => p.ValidUntil).First();
        
        return priceList;
    }

    private void DeleteOldestPriceList()
    {
        var priceList = _dbContext.PriceLists
            .Include(p => p.Legs)
                .ThenInclude(l => l.Providers)
            .Include(p => p.Legs)
                .ThenInclude(l => l.RouteInfo)
            .Include(p => p.Reservations)
                .ThenInclude(r => r.ReservationProviders)
            .OrderBy(p => p.ValidUntil).FirstOrDefault();

        if (priceList == null) return;
    
        _dbContext.PriceLists.Remove(priceList);
        _dbContext.SaveChanges();
    }

    private PriceList RemoveDuplicateInstances(PriceList priceList)
    {
        var companies = _dbContext.Companies.ToList();
        var planets = _dbContext.Planets.ToList();

        foreach (var leg in priceList.Legs)
        {
            var planet = planets.FirstOrDefault(p => p.PlanetName == leg.RouteInfo.PlanetFrom.PlanetName);
            if (planet != null)
            {
                leg.RouteInfo.PlanetFrom = planet;
            }
            else
            {
                planets.Add(leg.RouteInfo.PlanetFrom);
            }
            
            planet = planets.FirstOrDefault(p => p.PlanetName == leg.RouteInfo.PlanetTo.PlanetName);
            if (planet != null)
            {
                leg.RouteInfo.PlanetTo = planet;
            }
            else
            {
                planets.Add(leg.RouteInfo.PlanetTo);
            }
        }
        
        foreach (var provider in priceList.Legs.SelectMany(l => l.Providers))
        {
            var company = companies.FirstOrDefault(c => c.CompanyName == provider.Company.CompanyName);
            if (company != null)
            {
                provider.Company = company;
            }
            else
            {
                companies.Add(provider.Company);
            }
        }
        return priceList;
    }

}