﻿using Domain;

namespace Helpers;

public class FlightFinder
{
    private List<List<Leg>> _routes;
    private List<List<Provider>> _flights;
    private readonly Dictionary<Planet, List<Leg>> _routesFromPlanet;

    public FlightFinder(PriceList priceList)
    {
        _flights = new List<List<Provider>>();
        _routes = new List<List<Leg>>();
        _routesFromPlanet = new Dictionary<Planet, List<Leg>>();
        
        foreach (var leg in priceList.Legs)
        {
            if (!_routesFromPlanet.ContainsKey(leg.RouteInfo.PlanetFrom))
            {
                _routesFromPlanet[leg.RouteInfo.PlanetFrom] = new List<Leg>();
            }
            _routesFromPlanet[leg.RouteInfo.PlanetFrom].Add(leg);
        }
    }
    
    private void GetAllRoutesUtil(Planet current, Planet destination, IDictionary<Planet, bool> isVisited, ICollection<Leg> localPathList)
    {
        if (current.Equals(destination))
        {
            var path = localPathList.ToList();
            _routes.Add(path);
            return;
        }
        isVisited[current] = true;
 
        foreach(var legFromCurrent in _routesFromPlanet[current])
        {
            var legDestination = legFromCurrent.RouteInfo.PlanetTo;
            
            if (isVisited.ContainsKey(legDestination) && isVisited[legDestination]) continue;
            
            localPathList.Add(legFromCurrent);
            GetAllRoutesUtil(legDestination, destination, isVisited, localPathList);
            localPathList.Remove(legFromCurrent);
        }
 
        isVisited[current] = false;
    }
    
    private void GetAllFlightCombosUtil(int routeIndex, ICollection<Provider> localFlightCombo)
    {
        if (localFlightCombo.Count == _routes[routeIndex].Count)
        {
            var providers = localFlightCombo.ToList();
            _flights.Add(providers);
            return;
        }

        var leg = _routes[routeIndex][localFlightCombo.Count];
        
        var nextFlightOptions = localFlightCombo.Any()
            ? leg.Providers.Where(p => p.FlightStart > localFlightCombo.Last().FlightEnd)
            : leg.Providers;
        
        if (!nextFlightOptions.Any())
        {
            return;
        }
        
        foreach (var flight in nextFlightOptions)
        {
            localFlightCombo.Add(flight);
            GetAllFlightCombosUtil(routeIndex, localFlightCombo);
            localFlightCombo.Remove(flight);
        }
    }
    
    public List<List<Leg>> GetAllPossibleRoutesFromTo(Planet origin, Planet destination)
    {
        _routes = new List<List<Leg>>();
        
        var isVisited = new Dictionary<Planet, bool>();
        var pathList = new List<Leg>();
        
        GetAllRoutesUtil(origin, destination, isVisited, pathList);
        return _routes;
    }

    public List<List<Provider>> GetAllFlightCombinationsFromTo(Planet origin, Planet destination)
    {
        _flights = new List<List<Provider>>();
        GetAllPossibleRoutesFromTo(origin, destination);
        for (var i = 0; i < _routes.Count; i++)
        {
            GetAllFlightCombosUtil(i, new List<Provider>());
        }

        return _flights;
    }
    
}