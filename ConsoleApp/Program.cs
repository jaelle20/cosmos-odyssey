﻿// See https://aka.ms/new-console-template for more information

using DAL;
using Helpers;

// FOR BACKEND TESTING

var dbContext = new AppDbContext();
var controller = new DataController(dbContext);

// var pricelist = controller.GetActivePriceList();
var pricelist = controller.GetActivePriceList();

var routesFinder = new FlightFinder(pricelist);

var origin = pricelist.Legs.Select(l => l.RouteInfo.PlanetFrom).First(p => p.PlanetName == "Saturn");
var destination = pricelist.Legs.Select(l => l.RouteInfo.PlanetFrom).First(p => p.PlanetName == "Venus");

// var routes = routesFinder.GetAllPossibleRoutesFromTo(origin, destination);
// foreach (var path in routes)
// {
//     foreach (var leg in path)
//     {
//         var route = leg.RouteInfo;
//         Console.WriteLine(route.PlanetFrom.PlanetName + "->" + route.PlanetTo.PlanetName);
//     }
//     Console.WriteLine("------------");
// }

var flights = routesFinder.GetAllFlightCombinationsFromTo(origin, destination);

// var companyNames = new List<string>()
// {
//     "SpaceX",
//     "Galaxy Express",
//     "Explore Origin"
// };
// var topTen = flights
//     .Where(l => l.All(p => companyNames.Contains(p.Company.CompanyName)))
//     .OrderBy(l => l.Select(p => p.Price).Sum())
//     .Take(10); // Cheapest by selected companies

var topTen = flights
    .OrderBy(l => l.Select(p => p.FlightEnd).Max())
    .Take(10); // By arrival time

if (!topTen.Any())
{
    Console.WriteLine("None found");
}
else
{
    foreach (var flightcombo in topTen)
    {
        foreach (var flight in flightcombo)
        {
            Console.Write(flight.Price);
            Console.Write("\t");
            Console.Write(flight.Company.CompanyName);
            Console.Write("\t");
            Console.Write(flight.FlightStart);
            Console.Write("\t");
            Console.WriteLine(flight.FlightEnd);
        }
        Console.WriteLine(flightcombo.Select(p => p.Price).Sum());
        Console.WriteLine("===========");
    }
}
// Console.WriteLine(flights.Count);

// var json = JsonSerializer.Serialize(pricelist);
// var id = controller.DeleteOldestPriceList();
// Console.WriteLine(id);
