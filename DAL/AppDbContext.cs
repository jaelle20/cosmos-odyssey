﻿using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL;

public class AppDbContext : DbContext
{
    public DbSet<Company> Companies { get; set; }
    public DbSet<Leg> Legs { get; set; }
    public DbSet<Planet> Planets { get; set; }
    public DbSet<PriceList> PriceLists { get; set; }
    public DbSet<Provider> Providers { get; set; }
    public DbSet<Reservation> Reservations { get; set; }
    public DbSet<ReservationProvider> ReservationProviders { get; set; }
    public DbSet<RouteInfo> RouteInfos { get; set; }
    
    public AppDbContext(DbContextOptions<AppDbContext> options): base(options)
    {
    }

}